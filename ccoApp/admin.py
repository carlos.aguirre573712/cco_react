from django.contrib import admin
from .models.usuario import Usuario
from .models.almacen import Almacen
from .models.garantia import Garantia
from .models.pqr import Pqr
from .models.respuesta import Respuesta
from .models.rol import Rol


# REGISTRANDO DE TODOS LOS MODELOS
admin.site.register(Usuario)
admin.site.register(Almacen)
admin.site.register(Garantia)
admin.site.register(Pqr)
admin.site.register(Respuesta)
admin.site.register(Rol)
