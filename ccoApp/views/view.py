from django.shortcuts import render


def View(request, *args, **kwargs):
    return render(request, "index.html")
