from ccoApp.models import Rol
from rest_framework import status, views, generics, viewsets
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from ccoApp.serializers.rolSerializer import RolSerializer


class RolView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        rol = Rol.objects.get(id=kwargs["pk"])
        serializer_pac = RolSerializer(rol)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data_rol = request.data
        serializer_rol = RolSerializer(data=data_rol)
        serializer_rol.is_valid(raise_exception=True)
        rol = serializer_rol.save()

        return_data = {"rol": RolSerializer(rol).data}
        return Response(return_data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        rol = Rol.objects.filter(id=kwargs["pk"]).first()
        rol.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        rol = Rol.objects.get(id=kwargs["pk"])

        data_rol = request.data
        serializer_rol = RolSerializer(rol, data=data_rol)
        serializer_rol.is_valid(raise_exception=True)
        rol = serializer_rol.save()
        return_data = {"rol": RolSerializer(rol).data}
        return Response(return_data, status=status.HTTP_201_CREATED)


class AllRols(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Rol.objects.all()
    serializer_class = RolSerializer

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = RolSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
