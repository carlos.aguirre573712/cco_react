from ccoApp.models import Garantia
from rest_framework import status, views, generics, viewsets
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from ccoApp.serializers.garantiaSerializer import GarantiaSerializer


class GarantiaView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        garantia = Garantia.objects.get(id_Garantia=kwargs["pk"])
        serializer_pac = GarantiaSerializer(garantia)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data_garantia = request.data
        # data_garantia["Garantia"] = Garantia.EnumRol.USER
        serializer_garantia = GarantiaSerializer(data=data_garantia)
        serializer_garantia.is_valid(raise_exception=True)
        garantia = serializer_garantia.save()

        return_data = {"Garantia": GarantiaSerializer(garantia).data}
        return Response(return_data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        garantia = Garantia.objects.filter(id_Garantia=kwargs["pk"]).first()
        garantia.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        garantia = Garantia.objects.get(id_Garantia=kwargs["pk"])
        data_garantia = request.data
        serializer_garantia = GarantiaSerializer(garantia, data=data_garantia)
        serializer_garantia.is_valid(raise_exception=True)
        garantia = serializer_garantia.save()
        return_data = {"Garantia": GarantiaSerializer(garantia).data}
        return Response(return_data, status=status.HTTP_201_CREATED)


class AllGarantia(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Garantia.objects.all()
    serializer_class = GarantiaSerializer

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = GarantiaSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
