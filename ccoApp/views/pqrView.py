from ccoApp.models import Pqr
from rest_framework import status, views, generics, viewsets
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import PermissionDenied

from ccoApp.serializers.pqrSerializer import PqrSerializer


class PqrView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user = self.request.user
        pqr = Pqr.objects.filter(usuario_id_id=user)
        serializer_pac = PqrSerializer(pqr, many=True)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    """  pqr = Pqr.objects.get(id=kwargs["pk"])
        serializer_pac = PqrSerializer(pqr)
        return Response(serializer_pac.data, status=status.HTTP_200_OK) """

    def post(self, request, *args, **kwargs):
        data_pqr = request.data
        serializer_pqr = PqrSerializer(data=data_pqr)
        serializer_pqr.is_valid(raise_exception=True)
        pqr = serializer_pqr.save()

        return_data = {"PQR": PqrSerializer(pqr).data}
        return Response(return_data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        pqr = Pqr.objects.filter(id=kwargs["pk"]).first()
        pqr.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        # print("********************************************")
        userId = self.request.user.id
        # print("user: ", userId)

        pqr = Pqr.objects.get(id=kwargs["pk"])

        # print("********************************************")
        putUser = pqr.usuario_id_id
        # print("putUser: ", putUser)

        if userId == putUser:
            data_pqr = request.data
            serializer_pqr = PqrSerializer(pqr, data=data_pqr)
            serializer_pqr.is_valid(raise_exception=True)
            pqr = serializer_pqr.save()
            return_data = {"PQR": PqrSerializer(pqr).data}
            return Response(return_data, status=status.HTTP_200_OK)

        else:
            response = {
                "success": False,
                "status_code": status.HTTP_403_FORBIDDEN,
                "message": "No tienes permiso para modificar este objeto",
            }
            raise PermissionDenied(response)


class AllPqr(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Pqr.objects.all()
    serializer_class = PqrSerializer

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = PqrSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PqrId(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        userId = self.request.user.id
        pqr = Pqr.objects.get(id=kwargs["pk"])
        putUser = pqr.usuario_id_id

        if userId == putUser:
            pqr = Pqr.objects.get(id=kwargs["pk"])
            serializer_pac = PqrSerializer(pqr)
            return Response(serializer_pac.data, status=status.HTTP_200_OK)
        else:
            response = {
                "success": False,
                "status_code": status.HTTP_403_FORBIDDEN,
                "message": "No tienes permiso para visializar este objeto",
            }
            raise PermissionDenied(response)
