from ccoApp.models import Respuesta
from rest_framework import status, views, generics, viewsets
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from ccoApp.serializers.respuestaSerializer import RespuestaSerializer


class RespuestaView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        respuesta = Respuesta.objects.get(id_respuesta=kwargs["pk"])
        serializer_pac = RespuestaSerializer(respuesta)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data_respuesta = request.data
        serializer_respuesta = RespuestaSerializer(data=data_respuesta)
        serializer_respuesta.is_valid(raise_exception=True)
        respuesta = serializer_respuesta.save()
        return_data = {"Respuesta": RespuestaSerializer(respuesta).data}
        return Response(return_data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        respuesta = Respuesta.objects.filter(id_respuesta=kwargs["pk"]).first()
        respuesta.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        respuesta = Respuesta.objects.get(id_respuesta=kwargs["pk"])
        data_respuesta = request.data
        serializer_respuesta = RespuestaSerializer(respuesta, data=data_respuesta)
        serializer_respuesta.is_valid(raise_exception=True)
        respuesta = serializer_respuesta.save()
        return_data = {"Respuesta": RespuestaSerializer(respuesta).data}
        return Response(return_data, status=status.HTTP_201_CREATED)


class AllRespuesta(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Respuesta.objects.all()
    serializer_class = RespuestaSerializer

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = RespuestaSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
