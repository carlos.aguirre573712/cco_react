from ccoApp.models import Usuario
from rest_framework import status, views, generics, viewsets
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from ccoApp.serializers.usuarioSerializer import UsuarioSerializer


class UsuarioCreate(views.APIView):
    # Método para manejar la solicitud POST
    def post(self, request, *args, **kwargs):
        # Obtener datos de usuario del cuerpo de la solicitud
        data_usuario = request.data
        # Establecer el rol del usuario como USER
        data_usuario["Usuario"] = Usuario.EnumRol.USER
        # Serializar los datos del usuario y realizar validación
        serializer_user = UsuarioSerializer(data=data_usuario)
        serializer_user.is_valid(raise_exception=True)
        # Guardar el nuevo usuario y devolver la respuesta
        user = serializer_user.save()

        tokenData = {
            "Cedula": request.data["Cedula"],
            "password": request.data["password"],
        }
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)

        # return_data = {"Usuario": UsuarioSerializer(user).data}

        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)
