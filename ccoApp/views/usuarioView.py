from ccoApp.models import Usuario
from rest_framework import status, views, generics, viewsets, permissions
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import PermissionDenied


from ccoApp.serializers.usuarioSerializer import UsuarioSerializer


# Definición de la clase UsuarioView que hereda de views.APIView


class UsuarioView(views.APIView):
    permission_classes = (IsAuthenticated,)

    # Método para manejar la solicitud GET
    def get(self, request, *args, **kwargs):
        # Obtener un usuario
        user = self.request.user

        # Serializar el usuario y devolver la respuesta
        serializer_pac = UsuarioSerializer(user)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    # Método para manejar la solicitud DELETE
    def delete(self, request, *args, **kwargs):
        # Buscar y eliminar un usuario por su ID
        user = self.request.user
        user.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)
    # Método para manejar la solicitud PUT
    def put(self, request, *args, **kwargs):
        # Obtener un usuario por su ID
        user = self.request.user
        # Obtener datos de usuario del cuerpo de la solicitud
        data_usuario = request.data
        # Serializar los datos del usuario y realizar validación
        serializer_user = UsuarioSerializer(user, data=data_usuario)
        serializer_user.is_valid(raise_exception=True)
        # Guardar los cambios y devolver la respuesta
        user = serializer_user.save()
        return_data = {"Usuario": UsuarioSerializer(user).data}
        return Response(return_data, status=status.HTTP_201_CREATED)


# Definición de la clase AllUsuario que hereda de generics.ListAPIView Para obtener a todos los usuarios
class AllUsuario(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        # Obtén el usuario actual desde el token JWT
        user = self.request.user

        if user.Rol_id != 1:
            response = {
                "success": False,
                "status_code": status.HTTP_403_FORBIDDEN,
                "message": "You are not authorized to perform this action",
            }
            raise PermissionDenied(response)

        # Return all users if the role is 1
        return Usuario.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = UsuarioSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UsuarioId(views.APIView):
    permission_classes = (IsAuthenticated,)

    # Método para manejar la solicitud GET
    def get(self, request, *args, **kwargs):
        # Obtener un usuario por su ID
        user =self.request.user
        userQuery = Usuario.objects.get(id=kwargs["pk"])
        
        if user.Rol_id != 1:
            response = {
                "success": False,
                "status_code": status.HTTP_403_FORBIDDEN,
                "message": "You are not authorized to perform this action",
            }
            raise PermissionDenied(response)

        # Serializar el usuario y devolver la respuesta
        serializer_pac = UsuarioSerializer(userQuery)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    # Método para manejar la solicitud DELETE
    def delete(self, request, *args, **kwargs):
        user =self.request.user
       
        # Buscar y eliminar un usuario por su ID
        userQuery = Usuario.objects.filter(id=kwargs["pk"]).first()
        if user.Rol_id != 1:
            response = {
                "success": False,
                "status_code": status.HTTP_403_FORBIDDEN,
                "message": "You are not authorized to perform this action",
            }
            raise PermissionDenied(response)
        
        userQuery.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)

    # Método para manejar la solicitud PUT
    def put(self, request, *args, **kwargs):
        user =self.request.user
        # Obtener un usuario por su ID
        userQuery = Usuario.objects.get(id=kwargs["pk"])
        if user.Rol_id != 1:
            response = {
                "success": False,
                "status_code": status.HTTP_403_FORBIDDEN,
                "message": "You are not authorized to perform this action",
            }
            raise PermissionDenied(response)
        # Obtener datos de usuario del cuerpo de la solicitud
        data_usuario = request.data
        # Serializar los datos del usuario y realizar validación
        serializer_user = UsuarioSerializer(userQuery, data=data_usuario)
        serializer_user.is_valid(raise_exception=True)
        # Guardar los cambios y devolver la respuesta
        userQuery = serializer_user.save()
        return_data = {"Usuario": UsuarioSerializer(userQuery).data}
        return Response(return_data, status=status.HTTP_201_CREATED)
