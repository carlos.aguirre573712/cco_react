from wsgiref import validate
from django.conf import settings
from ccoApp.serializers import usuarioSerializer
from ccoApp.serializers.usuarioSerializer import UsuarioSerializer
from rest_framework import status, views, generics
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from datetime import datetime
from ccoApp.models import Usuario


class ValidacionView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        token = request.META.get("HTTP_AUTHORIZATION")[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT["ALGORITHM"])
        valid_data = tokenBackend.decode(token, verify=False)
        print(valid_data)

        userModel = Usuario.objects.get(id=valid_data["user_id"])
        usuarioSerializer = UsuarioSerializer(userModel)
        data = usuarioSerializer.data
        stringResponse = {"rolUser": data["rol"]}
        return Response(stringResponse, status=status.HTTP_200_OK)
