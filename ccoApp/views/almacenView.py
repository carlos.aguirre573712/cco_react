from django.conf import settings
from ccoApp.models import Almacen
from rest_framework import status, views, generics, viewsets
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated

from ccoApp.serializers.almacenSerializer import AlmacenSerializer


class AlmacenView(views.APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        almacen = Almacen.objects.get(id=kwargs["pk"])
        serializer_pac = AlmacenSerializer(almacen)
        return Response(serializer_pac.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data_almacen = request.data
        serializer_almacen = AlmacenSerializer(data=data_almacen)
        serializer_almacen.is_valid(raise_exception=True)
        almacen = serializer_almacen.save()

        return_data = {"Usuario": AlmacenSerializer(almacen).data}
        return Response(return_data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        almacen = Almacen.objects.filter(id=kwargs["pk"]).first()
        almacen.delete()
        stringResponse = {"detail": "Registro Eliminado exitosamente"}
        return Response(stringResponse, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        almacen = Almacen.objects.get(id=kwargs["pk"])
        data_almacen = request.data
        serializer_almacen = AlmacenSerializer(almacen, data=data_almacen)
        serializer_almacen.is_valid(raise_exception=True)
        almacen = serializer_almacen.save()
        return_data = {"Almacen": AlmacenSerializer(almacen).data}
        return Response(return_data, status=status.HTTP_201_CREATED)


class AllAlmacen(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Almacen.objects.all()
    serializer_class = AlmacenSerializer

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = AlmacenSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
