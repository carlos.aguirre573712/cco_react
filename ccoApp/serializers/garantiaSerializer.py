from rest_framework import serializers
from ccoApp.models.garantia import Garantia
from ccoApp.models.usuario import Usuario
from ccoApp.models.almacen import Almacen


class GarantiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garantia
        fields = ["id_Garantia", "Cod_Producto", "Id_Usuario"]

    def create(self, validated_data):
        GarantiaInstance = Garantia.objects.create(**validated_data)
        return GarantiaInstance

    def to_representation(self, obj):
        garantia = Garantia.objects.get(id_Garantia=obj.id_Garantia)
        user = Usuario.objects.get(id=garantia.Id_Usuario_id)
        almacen = Almacen.objects.get(id=garantia.Cod_Producto_id)
        return {
            "id_Garantia": garantia.id_Garantia,
            "Producto": {
                "Cod_Producto": almacen.Cod_Producto,
                "Fecha_ingreso": almacen.Fecha_ingreso,
                "Fecha_Devolucion": almacen.Fecha_Devolucion,
                "Categoria": almacen.categoria,
            },
            "Usuario": {
                "id": user.id,
                "Nombres": user.Nombres,
                "Apellidos": user.Apellidos,
                "Rol": user.Rol.Nombre_Rol,
                "Celular": user.Celular,
                "Email": user.Email,
                "Cedula": user.Cedula,
            },
        }
