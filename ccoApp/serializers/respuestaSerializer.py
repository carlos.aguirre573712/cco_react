from rest_framework import serializers
from ccoApp.models.pqr import Pqr
from ccoApp.models.respuesta import Respuesta
from ccoApp.models.usuario import Usuario


class RespuestaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respuesta
        fields = [
            "id_respuesta",
            "pqr",
            "respuesta",
        ]

    def create(self, validated_data):
        RespuestaInstance = Respuesta.objects.create(**validated_data)
        return RespuestaInstance

    def to_representation(self, obj):
        respuesta = Respuesta.objects.get(id_respuesta=obj.id_respuesta)
        pqr = Pqr.objects.get(id=respuesta.pqr_id)
        user = Usuario.objects.get(id=pqr.usuario_id_id)
        return {
            "id_respuesta": respuesta.id_respuesta,
            "respuesta": respuesta.respuesta,
            "pqr": {
                "id": pqr.id,
                "fecha_devolucion": pqr.fecha_devolucion,
                "fecha_creacion": pqr.fecha_creacion,
                "observacion": pqr.observacion,
                "contenido": pqr.contenido,
                "Usuario": user.Nombres,
                "estado": pqr.estado,
                "tipo_pqr": pqr.tipo_pqr,
                "id_garantia": pqr.id_garantia_id,
            },
        }
