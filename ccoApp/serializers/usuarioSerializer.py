from rest_framework import serializers
from ccoApp.models.rol import Rol
from ccoApp.models.usuario import Usuario


# Esta clase es un serializador para la clase Usuario.
class UsuarioSerializer(serializers.ModelSerializer):
    # La clase Meta especifica los campos que se serializarán.
    class Meta:
        model = Usuario
        fields = [
            "Nombres",
            "Apellidos",
            "Rol",
            "Celular",
            "Email",
            "Cedula",
            "password",
        ]

    def create(self, validated_data):
        # Este método crea una nueva instancia de Almacen.
        UsuarioInstance = Usuario.objects.create(**validated_data)
        return UsuarioInstance

    def to_representation(self, obj):
        # Este método devuelve una representación de la instancia de Almacen.
        user = Usuario.objects.get(id=obj.id)
        rol = Rol.objects.get(id=user.Rol_id)
        return {
            "id": user.id,
            "Nombres": user.Nombres,
            "Apellidos": user.Apellidos,
            "Rol": {rol.Nombre_Rol},
            "Celular": user.Celular,
            "Email": user.Email,
            "Cedula": user.Cedula,
        }
