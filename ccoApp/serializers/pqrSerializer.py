from rest_framework import serializers
from ccoApp.models.pqr import Pqr
from ccoApp.models.usuario import Usuario
from ccoApp.models.garantia import Garantia


class PqrSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pqr
        fields = [
            "id",
            "fecha_devolucion",
            "fecha_creacion",
            "observacion",
            "contenido",
            "usuario_id",
            "estado",
            "tipo_pqr",
            "id_garantia",
        ]

    def create(self, validated_data):
        PqrInstance = Pqr.objects.create(**validated_data)
        return PqrInstance

    def to_representation(self, obj):
        pqr = Pqr.objects.get(id=obj.id)
        user = Usuario.objects.get(id=pqr.usuario_id_id)

        print(pqr.id_garantia_id)

        if pqr.id_garantia_id == None:
            garantia2 = {
                "id_Garantia": None,
                "Producto": None,
                "Usuario": None,
            }

        else:
            garantia = Garantia.objects.get(id_Garantia=pqr.id_garantia_id)

            garantia2 = {
                "id_Garantia": garantia.id_Garantia,
                "Producto": garantia.Cod_Producto_id,
                "Usuario": garantia.Id_Usuario_id,
            }

        print(garantia2)
        return {
            "id": pqr.id,
            "fecha_devolucion": pqr.fecha_devolucion,
            "fecha_creacion": pqr.fecha_creacion,
            "observacion": pqr.observacion,
            "contenido": pqr.contenido,
            "Usuario": {
                "id": user.id,
                "Nombres": user.Nombres,
                "Apellidos": user.Apellidos,
                "Rol": user.Rol.Nombre_Rol,
                "Celular": user.Celular,
                "Email": user.Email,
                "Cedula": user.Cedula,
            },
            "estado": pqr.estado,
            "tipo_pqr": pqr.tipo_pqr,
            "id_garantia": garantia2,
        }
