from rest_framework import serializers
from ccoApp.models.almacen import Almacen

# Esta clase es un serializador para la clase Almacen.


class AlmacenSerializer(serializers.ModelSerializer):
    # La clase Meta especifica los campos que se serializarán.
    class Meta:
        model = Almacen
        fields = "__all__"

    def create(self, validated_data):
        # Este método crea una nueva instancia de Almacen.
        AlmacenInstance = Almacen.objects.create(**validated_data)
        return AlmacenInstance

    def to_representation(self, obj):
        # Este método devuelve una representación de la instancia de Almacen.
        almacen = Almacen.objects.get(Cod_Producto=obj.Cod_Producto)
        return {
            "id": almacen.id,
            "Cod_Producto": almacen.Cod_Producto,
            "Fecha_ingreso": almacen.Fecha_ingreso,
            "Fecha_Devolucion": almacen.Fecha_Devolucion,
            "categoria": almacen.categoria,
        }
