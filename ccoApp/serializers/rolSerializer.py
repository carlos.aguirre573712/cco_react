from rest_framework import serializers
from ccoApp.models.rol import Rol


class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rol
        fields = ["id", "Nombre_Rol"]

    def create(self, validated_data):
        RolInstance = Rol.objects.create(**validated_data)
        return RolInstance

    def to_representation(self, obj):
        rol = Rol.objects.get(id=obj.id)
        return {
            "id": rol.id,
            "Nombre_Rol": rol.Nombre_Rol,
        }
