from .usuarioSerializer import UsuarioSerializer
from .rolSerializer import RolSerializer

from .almacenSerializer import AlmacenSerializer
from .garantiaSerializer import GarantiaSerializer
from .pqrSerializer import PqrSerializer
from .respuestaSerializer import RespuestaSerializer
