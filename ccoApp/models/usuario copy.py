""" from django.db import models
from .rol import Rol

#  DEFINICION DEL MODELO USUARIO PARA LA BASE DE DATOS


class Usuario(models.Model):
    class EnumRol(models.TextChoices):
        ADMIN = "1"
        USER = "2"

    id = models.BigAutoField(primary_key=True)
    Nombres = models.CharField(max_length=100)
    Apellidos = models.CharField(max_length=100)
    Rol = models.ForeignKey(
        Rol,
        related_name="id_Rol",
        choices=EnumRol.choices,
        default=EnumRol.USER,
        on_delete=models.RESTRICT,
    )
    Celular = models.CharField(max_length=20)
    Email = models.EmailField()
    Cedula = models.CharField(max_length=20, unique=True)
    Password = models.CharField(max_length=100)
 """
