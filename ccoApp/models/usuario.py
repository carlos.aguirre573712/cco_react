from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    BaseUserManager,
)
from django.contrib.auth.hashers import make_password
from .rol import Rol


#  DEFINICION DEL MODELO USER MANAGER
class UserManager(BaseUserManager):
    def create_user(self, Cedula, password=None, **extra_fields):
        if not Cedula:
            raise ValueError("Los usuarios deben tener un username")
        user = self.model(Cedula=Cedula, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, Cedula, password, **extra_fields):
        extra_fields.setdefault("Rol", Rol.objects.get(id=1))
        user = self.create_user(Cedula=Cedula, password=password, **extra_fields)
        if extra_fields.get("Rol") != Rol.objects.get(id=1):
            raise ValueError("Superuser must have role of Global Admin")
        user.is_admin = True
        user.save(using=self._db)
        return user


#  DEFINICION DEL MODELO USUARIO PARA LA BASE DE DATOS


class Usuario(AbstractBaseUser, PermissionsMixin):
    class EnumRol(models.TextChoices):
        ADMIN = "1"
        USER = "2"

    id = models.BigAutoField(primary_key=True)
    Nombres = models.CharField("Nombres", max_length=100)
    Apellidos = models.CharField("Apellidos", max_length=100)
    Rol = models.ForeignKey(
        Rol,
        related_name="id_Rol",
        choices=EnumRol.choices,
        default=EnumRol.USER,
        on_delete=models.RESTRICT,
    )
    Celular = models.CharField("Celular", max_length=20)
    Email = models.EmailField("Email", max_length=100)
    Cedula = models.CharField("Cedula", max_length=20, unique=True)
    password = models.CharField("Password", max_length=100)

    def save(self, **kwargs):
        some_salt = "mMUj0DrIK6vgtdIYepkIxN"
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = "Cedula"
