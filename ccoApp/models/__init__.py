from .usuario import Usuario
from .almacen import Almacen
from .garantia import Garantia
from .pqr import Pqr
from .respuesta import Respuesta
from .rol import Rol
