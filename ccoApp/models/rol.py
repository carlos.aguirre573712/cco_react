from django.db import models


#  DEFINICION DEL MODELO ROL PARA LA BASE DE DATOS
class Rol(models.Model):
    id = models.AutoField(
        primary_key=True, verbose_name="Identificador único Rol de usuarios"
    )
    Nombre_Rol = models.CharField(max_length=50, verbose_name="Nombre del rol")

    def __str__(self) -> str:
        return self.Nombre_Rol
