from django.db import models

#  DEFINICION DEL MODELO ALMACEN PARA LA BASE DE DATOS


class Almacen(models.Model):
    id = models.BigAutoField(primary_key=True)
    Cod_Producto = models.CharField(max_length=20, unique=True)
    Fecha_ingreso = models.DateField()
    Fecha_Devolucion = models.DateField(null=True)
    categoria = models.CharField(max_length=100, null=True)
