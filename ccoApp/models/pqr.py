from django.db import models
from .usuario import Usuario
from .garantia import Garantia

#  DEFINICION DEL MODELO PQR PARA LA BASE DE DATOS


class Pqr(models.Model):
    id = models.BigAutoField(primary_key=True)
    fecha_devolucion = models.DateField(null=True)
    fecha_creacion = models.DateField(null=False)
    observacion = models.CharField(max_length=200, null=True)
    contenido = models.CharField(max_length=200, null=False)
    usuario_id = models.ForeignKey(
        Usuario, related_name="id_Usuario", on_delete=models.CASCADE
    )
    estado = models.CharField(
        max_length=10,
        choices=[
            ("ABIERTA", "ABIERTA"),
            ("CERRADA", "CERRADA"),
            ("EN PROCESO", "EN PROCESO"),
        ],
        default="ABIERTA",
    )
    tipo_pqr = models.CharField(
        max_length=10,
        choices=[("PQR", "PQR"), ("GARANTIA", "GARANTIA")],
        default="PQR",
    )
    id_garantia = models.ForeignKey(
        Garantia,
        on_delete=models.CASCADE,
        related_name="Id_Garantia",
        null=True,
    )
