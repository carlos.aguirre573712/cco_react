from django.db import models
from .usuario import Usuario
from .almacen import Almacen

#  DEFINICION DEL MODELO GARANTIA PARA LA BASE DE DATOS


class Garantia(models.Model):
    id_Garantia = models.BigAutoField(primary_key=True)
    Cod_Producto = models.ForeignKey(
        Almacen,
        related_name="id_Cod_Producto",
        max_length=50,
        null=False,
        blank=False,
        on_delete=models.RESTRICT,
    )
    Id_Usuario = models.ForeignKey(
        Usuario, on_delete=models.RESTRICT, related_name="Id_Usuario"
    )
