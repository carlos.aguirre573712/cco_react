from django.db import models
from ccoApp.models.pqr import Pqr


#  DEFINICION DEL MODELO RESPUESTA PARA LA BASE DE DATOS
class Respuesta(models.Model):
    # El id de la respuesta
    id_respuesta = models.AutoField(primary_key=True)

    # El id del PQR al que pertenece la respuesta
    pqr = models.ForeignKey(Pqr, related_name="id_PQR", on_delete=models.CASCADE)

    # La respuesta del PQR
    respuesta = models.CharField(max_length=200, blank=True, null=True)
