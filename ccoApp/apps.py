from django.apps import AppConfig


class CcoappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ccoApp'
