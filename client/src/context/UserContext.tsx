/* 
import { createContext, ReactNode, useState } from 'react'
import { useNavigate } from 'react-router-dom';

type Props = {
  children?: ReactNode;
}

type IAuthContext = {
  authenticated: boolean;
  setAuthenticated: (newState: boolean) => void
}

const initialValue = {
  authenticated: false,
  setAuthenticated: () => {}
}

const AuthContext = createContext<IAuthContext>(initialValue)

const AuthProvider = ({children}: Props) => {
  //Initializing an auth state with false value (unauthenticated)
  const [ authenticated, setAuthenticated ] = useState(initialValue.authenticated)

  const navigate = useNavigate()

  return (
    <AuthContext.Provider value={{authenticated, setAuthenticated}}>
      {children}
    </AuthContext.Provider>
  )
}

export {  AuthContext, AuthProvider } */

// import React, { createContext, useState } from "react";

// type AuthUser = {
//     cedula: string

// }

// export type UserContextType = {
//     cedula: any;
//     setCedula: any;
// }

// type UserContextProviderType = {
//     children: React.ReactNode
// }


// export const UserContext = createContext({} as UserContextType)

// export const UserContextProvider = ({ children }: UserContextProviderType) => {
//     const [cedula, setCedula] = useState<AuthUser | null>(null);
//     return (<UserContext.Provider value={{ cedula, setCedula }}>
//         {children}
//     </UserContext.Provider>
//     );
// }

import axios from "axios";
import { createContext, useContext, useEffect, useMemo, useState } from "react";

const BASE_URL = 'http://127.0.0.1:8000/ccoApp';

export type UserContextType = {
  token: string | null;
  setToken: (newToken?: string | null) => void;
  userData: any; // Add userData to the interface
  setUserData: (data: any) => void; // Add setUserData function
}

type UserContextProviderType = {
  children: React.ReactNode
}


const AuthContext = createContext({} as UserContextType)

const AuthProvider = ({ children }: UserContextProviderType) => {

  // State to hold the authentication token
  const [token, setToken_] = useState(localStorage.getItem("token"));
  const [userData, setUserData] = useState(localStorage.getItem("user"));

  // Function to set the authentication token
  const setToken = (newToken: any) => {
    setToken_(newToken);
  };
  // State to hold the user's cedula


  useEffect(() => {
    if (token) {
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
      localStorage.setItem('token', token);
      //console.log("token", token.refresh);

      axios.get(`${BASE_URL}/User/`, {
        headers: {
          Authorization: `Bearer ${token.access}`,
        },
      })
        .then((response) => {

          if (response.data) {
            localStorage.setItem('user', JSON.stringify(response.data)); // Guarda los datos en localStorage
            setUserData(response.data); // Guarda los datos del usuario en el estado
          }
          // ... otras acciones con los datos
        })
        .catch((error) => {
          console.error("Error al obtener los datos del usuario:", error);
        });

    } else {
      delete axios.defaults.headers.common["Authorization"];
      localStorage.removeItem('token')
    }
  }, [token]);

  // Memorized value of the authentication context
  const contextValue = useMemo(
    () => ({
      token,
      setToken,
      userData,
      setUserData

    }),
    [token, userData]
  );

  // Provide the authentication context to the children components
  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};

export default AuthProvider;