
import { Link } from 'react-router-dom';

const Soporte = () => {
  return (
    <>
      <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
        <div className="flex flex-wrap items-center">
          <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
            <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">




              <h2 className="mb-2 text-2xl  flex justify-left font-bold text-black dark:text-white sm:text-title-xl2">
                Preguntas frecuentes
              </h2>




              <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">

                <div className="flex flex-wrap items-center">
                  <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
                    <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">

                      <div className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Cómo crear una PQR?
                      </div>
                      <hr />
                      <p className="mb-12.5 text-1xl font-semibold text-black dark:text-white">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque odit perspiciatis at quae. Fugiat natus id delectus, aperiam possimus neque doloremque quisquam aliquam eveniet, quae, quas in commodi. Ipsam, ratione?
                      </p>

                      <div className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Como Consulto mis PQR?
                      </div>
                      <hr />
                      <p className="mb-12.5 text-1xl font-semibold text-black dark:text-white">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque odit perspiciatis at quae. Fugiat natus id delectus, aperiam possimus neque doloremque quisquam aliquam eveniet, quae, quas in commodi. Ipsam, ratione?
                      </p>
                      <div className="mb-.5 text-1xl font-semibold text-black dark:text-white">
                        No encuentro mis PQR
                      </div>
                      <hr />
                      <p className="mb-1.5 text-1xl font-semibold text-black dark:text-white">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque odit perspiciatis at quae. Fugiat natus id delectus, aperiam possimus neque doloremque quisquam aliquam eveniet, quae, quas in commodi. Ipsam, ratione?
                      </p>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div >
    </>
  );
};

export default Soporte;
