
import { Link } from 'react-router-dom';
import Banner from "../../images/cover/banner.png"
import { fetchDataWithToken } from '../../api/dataFetch';
import { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { useAuth } from '../../context/UserContext';


const BASE_URL = 'http://127.0.0.1:8000/ccoApp';


const Home = () => {

  // const [firstName, setFirstName] = useState<string>('');
  // const [lastName, setLastName] = useState<string>('');
  const [user, setUser] = useState<{ firstName: string; lastName: string }>({ firstName: '', lastName: '' });

  const { token } = useAuth();
  const { userData } = useAuth();

  // console.log("userData", userData);






  const fetchData = useCallback(async () => {
    try {
      console.log("conteo1");
      const response = await axios.get(`${BASE_URL}/User/`, {
        headers: {
          Authorization: `Bearer ${token.access}`,
        },
      });
      setUser({ firstName: response.data.Nombres || '', lastName: response.data.Apellidos || '' });
    } catch (error) {
      console.error(error);
    }
  }, [token]);

  useEffect(() => {
    fetchData();
    console.log("conteo");

  }, [fetchData]);



  return (
    <>
      <div>
        <img className="hidden dark:block" src={Banner} alt="Banner" />
        <img className="dark:hidden" src={Banner} alt="Banner" />
      </div>


      <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
        <div className="flex flex-wrap items-center">
          <div className="w-full border-stroke dark:border-strokedark xl:w-1/1 xl:border-l-2">
            <div className="w-full p-4 sm:p-12.5 xl:p-17.5 ">

              <h2 className="mb-9 text-2xl  flex justify-center font-bold text-black dark:text-white sm:text-title-xl2">
                BIENVENIDO
              </h2>

              <h1 className="mb-9 text-5xl flex justify-center font-bold text-black dark:text-white ">

                {user.firstName} {user.lastName}
              </h1>

              <h1 className="mb-2 text-2xl  flex justify-center font-bold text-black dark:text-white sm:text-title-xl2">
                Que deseas realizar:
              </h1>

            </div>
            <div className="mb-10 relative rounded-xl overflow-auto p-1">
              <div className='flex flex-wrap gap-4 items-center justify-center' >
                <div className="w-2/5 flex-none items-center justify-center last:pr-8 ">
                  <Link to="/forms/form-pqr" className="text-primary">
                    <button
                      type="button"
                      className="p-4 w-full  cursor-pointer rounded-lg border border-primary bg-primary text-white transition hover:bg-opacity-90"
                    >
                      Registrar PQRs
                    </button>
                  </Link>
                </div>
                <div className="w-2/5 flex-none items-center justify-center last:pr-8">
                  <Link to="/pqrs" className="text-primary">
                    <button
                      type="button"
                      className="p-4 w-full cursor-pointer rounded-lg border border-primary bg-primary text-white transition hover:bg-opacity-90"
                    >
                      Consultar PQRs
                    </button>
                  </Link>
                </div>
              </div>

            </div>








          </div>
        </div>

      </div >
      {/* <div className="grid grid-cols-1 gap-4 md:grid-cols-2 md:gap-6 xl:grid-cols-4 2xl:gap-7.5">
        <CardOne />
        <CardTwo />
        <CardThree />
        <CardFour />
      </div>

      <div className="mt-4 grid grid-cols-12 gap-4 md:mt-6 md:gap-6 2xl:mt-7.5 2xl:gap-7.5">
        <ChartOne />
        <ChartTwo />
        <ChartThree />
        <MapOne />
        <div className="col-span-12 xl:col-span-8">
          <TableOne />
        </div>
        <ChatCard />
      </div> */}
    </>
  );
};

export default Home;
