import { useNavigate } from "react-router-dom";
import { useAuth } from "../context/UserContext";

const Logout = () => {
    const { setToken } = useAuth();
    const navigate = useNavigate();

    const handleLogout = () => {
        setToken(null);
        navigate("/auth/signin", { replace: true });
    };

    setTimeout(() => {
        handleLogout();
    }, 3 * 1000);

    return <>Logout Page</>;
};

export default Logout;