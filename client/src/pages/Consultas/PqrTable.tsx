import Breadcrumb from '../../components/Breadcrumb';
import TableComponent from '../../components/Table';

// import TableThree from '../../components/TableThree';


const PqrTable = () => {
  return (
    <>
      <Breadcrumb pageName="Mis PQR" />

      <div className="flex flex-col gap-10">
        {/* <TableThree /> */}
        <TableComponent />
      </div>
    </>
  );
};
export default PqrTable;
/* import Breadcrumb from '../../components/Breadcrumb';

import TableThree from '../../components/TableThree';


const PqrTable = () => {
  return (
    <>
      <Breadcrumb pageName="Mis PQR" />

      <div className="flex flex-col gap-10">
        <TableThree />
      </div>
    </>
  );
};

export default PqrTable;
 */