import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { createPqr } from "../../api/createPqr";
import Breadcrumb from "../../components/Breadcrumb";
// import CurrentDate from "../../components/CurrentDate";
import { useAuth } from "../../context/UserContext";
import { useState } from "react";
import { jwtDecode } from 'jwt-decode';

const BASE_URL = 'http://127.0.0.1:8000/ccoApp';

const FormPqr = () => {

  const { token } = useAuth();

  const [userId, setUserId] = useState<string>('');

  const navigate = useNavigate();
  // const currentDate = CurrentDate();

  interface PqrForm {
    dateDev: Date | null,
    dateCreate: Date,
    observation: string,
    content: string,
    // userId: number,
    statePqr: string,
    typePqr: string,
    // garantyId: number,
  }

  // const [dateDev, setDateDev] = useState<string>('');
  // const [dateCreate, setDateCreate] = useState<string>('');
  // const [observation, setObservation] = useState<string>('');
  // const [content, setContent] = useState<string>('');
  // // const [userId, setUserId] = useState<string>('');
  // const [statePqr, setStatePqr] = useState<string>('');
  // const [typePqr, setTypePqr] = useState<string>('');
  // const [garantyId, setGarantyId] = useState<string>('');

  // const { register, handleSubmit, reset, setError, formState: { errors } } = useForm();
  const { register, handleSubmit, reset, setError, formState: { errors } } = useForm<PqrForm>();


  const onSubmit: SubmitHandler<PqrForm> = async (data) => {
    console.log("CLIC");
    console.log(data.dateCreate);

    try {



      // Obtener id  de  usuario
      // const responseUser = await axios.get(`${BASE_URL}/User/`, {
      //   headers: {
      //     Authorization: `Bearer ${token}`,
      //   },
      // });

      // console.log(responseUser.data);
      // setUserId(responseUser.data.id || '');
      // console.log('User', responseUser.data.id);

      const decodedToken = jwtDecode(token.access);

      // console.log(decodedToken.user_id);

      const response = await createPqr(data.dateCreate, data.observation, data.content, decodedToken.user_id, data.statePqr, data.typePqr, token.access);

      if (response) {
        //setToken(response);
        alert('PQR registrada correctamente');
        navigate("/home", { replace: true });
      } else {
        // Manejar el caso en el que no se obtiene el token
        alert('Un Campo  no es valido');
        console.error('Un Campo  no es valido');
        console.log(errors);
        reset();
      }
    } catch (error) {
      // Manejar errores
      console.error('Error al intentar registrar una PQR:', error);
      setError('typePqr', { type: 'manual', message: 'Alguno de los campos no es valido' });
      reset();
    }
  };


  return (
    <>
      <Breadcrumb pageName="Crear una PQR" />
      <div className="w-full p-4 sm:p-12.5 xl:p-17.5">
        <div className="flex flex-col gap-4 ">

          <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
              <h3 className="font-medium text-black dark:text-white">
                Crear PQR
              </h3>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="p-6.5 ">
                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">
                  <div className="w-full xl:w-1/2">
                    <div>
                      <label className="mb-3 block text-black dark:text-white">
                        Fecha de creacion
                      </label>
                      <div className="relative">
                        <input
                          type="date"
                          {...register('dateCreate', { required: 'Este campo es obligatorio' })}
                          className="custom-input-date custom-input-date-2 w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                          // disabled
                          // placeholder={(new Date().toISOString().split('T')[0])}
                          onChange={(e) => {
                            const selectedDate = new Date(e.target.value);
                            const formattedDate = selectedDate.toISOString().split('T')[0];
                          }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="w-full xl:w-1/2">
                    <div>
                      <label className="mb-3 block text-black dark:text-white">
                        Fecha de Devolución
                      </label>
                      <div className="relative">
                        <input
                          type="date"
                          {...register('dateDev', {})}
                          className="custom-input-date custom-input-date-2 w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                          // onChange={(e) => {
                          //   const selectedDate = new Date(e.target.value);
                          //   const formattedDate = selectedDate.toISOString().split('T')[0];
                          // }}
                          disabled
                        />
                      </div>
                    </div>
                  </div>
                </div>


                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">

                  <div className="w-full xl:w-1/2">
                    <label className="mb-3 block text-black dark:text-white">
                      Seleccione Tipo de Consulta
                    </label>
                    <div className="relative z-20 bg-white dark:bg-form-input">

                      <span className="absolute top-1/2 left-4 z-30 -translate-y-1/2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24" fill="none" stroke="#b1b9c5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><path d="M13 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V9l-7-7z" /><path d="M13 3v6h6" /></svg>
                      </span>
                      <select
                        // {...register('typePqr')}
                        className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-12 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                        <option value="">PQR</option>
                        <option value="">GARANTIA</option>
                      </select>
                      <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <g opacity="0.8">
                            <path
                              fillRule="evenodd"
                              clipRule="evenodd"
                              d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                              fill="#637381"
                            ></path>
                          </g>
                        </svg>
                      </span>
                    </div>
                  </div>



                  <div className="w-full xl:w-1/2">
                    <label className="mb-2.5 block text-black dark:text-white">
                      Estado
                    </label>
                    <input
                      type="text"
                      // {...register('statePqr', { required: 'Este campo es obligatorio' })}
                      //placeholder="ABIERTA"
                      disabled
                      value={"ABIERTA"}
                      className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary dark:disabled:bg-black"
                    />
                  </div>
                </div>

                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">


                </div>


                <div className="mb-6">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Contenido
                  </label>
                  <textarea
                    rows={6}
                    {...register('content', { required: 'Este campo es obligatorio' })}
                    placeholder="Type your message"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  ></textarea>
                </div>


                <div className="mb-6">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Observaciones
                  </label>
                  <textarea
                    rows={6}
                    {...register('observation', { required: 'Este campo es obligatorio' })}
                    placeholder="Type your message"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  ></textarea>
                </div>

                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">
                  <input
                    type="submit"
                    value="Crear"
                    className="flex w-full cursor-pointer justify-center rounded bg-primary p-3 font-medium text-gray"
                  />
                  <button className="flex w-full justify-center rounded bg-primary p-3 font-medium text-gray"
                    onClick={() => reset()}>
                    Cancelar
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div >

      </div >
    </>
  );
};

export default FormPqr;
