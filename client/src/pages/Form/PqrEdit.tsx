import { useNavigate, useParams } from "react-router-dom";
import Breadcrumb from "../../components/Breadcrumb";
// import CurrentDate from "../../components/CurrentDate";
import { useAuth } from "../../context/UserContext";
import { useEffect, useState } from "react";
import { jwtDecode } from 'jwt-decode';
import axios from "axios";
import Pqr from "../Consultas/Pqr";
import { editPqr } from "../../api/editPqr";
import { SubmitHandler, useForm } from "react-hook-form";

const BASE_URL = 'http://127.0.0.1:8000/ccoApp';

const FormPqrEdit = () => {

  const { token } = useAuth();
  const { userData } = useAuth();
  console.log(userData.Rol[0]);

  const [userId, setUserId] = useState<string>('');
  const navigate = useNavigate();

  const [data, setData] = useState({
    id: '',
    fecha_devolucion: '',
    fecha_creacion: '',
    observacion: '',
    contenido: '',
    Usuario: {
      id: '',
      Nombres: '',
      Apellidos: '',
      Rol: '',
      Celular: '',
      Email: '',
      Cedula: ''
    },
    estado: '',
    tipo_pqr: '',
    id_garantia: { id_Garantia: '', Producto: '', Usuario: '' }
  });
  // const [text, setText] = useState(data.observacion);

  const { pqr_id } = useParams();
  console.log("hola pqr_id", pqr_id);



  useEffect(() => {
    const obtenerDatos = async () => {
      try {
        const respuesta = await axios.get(`${BASE_URL}/PqrId/${pqr_id}`, {
          headers: {
            Authorization: `Bearer ${token.access}`,
          },
        });

        // Actualiza data solo si ha cambiado
        if (JSON.stringify(data) !== JSON.stringify(respuesta.data)) {
          setData(respuesta.data);
        }

        console.log(respuesta.data);
      } catch (error) {
        console.error(error);
      }
    };

    obtenerDatos();
  }, [token, pqr_id]); // Agrega data como dependencia


  // Función para manejar los cambios en el formulario
  // const handleChange = (event: { target: { name: any; value: any; }; }) => {
  //   setData({
  //     ...data,
  //     [event.target.name]: event.target.value
  //   });
  // };

  const handleChange = (event: { target: { name: any; value: any; }; }) => {
    let value = event.target.value;

    // Verificar si el valor es una fecha
    if (event.target.name === 'fecha_devolucion') {
      const selectedDate = new Date(value);
      value = selectedDate.toISOString().split('T')[0];
    }

    setData({
      ...data,
      [event.target.name]: value
    });
  };

  // Función para manejar el envío del formulario
  const handleSubmit = async (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    // axios.post('/ruta/a/tu/base/de/datos', data)
    //   .then(response => {
    //     console.log(response);
    //   });
    console.log("CLIC UPDATE");
    console.log(data);
    try {
      const decodedToken = jwtDecode(token.access);
      console.log(data.fecha_devolucion);
      const response = await editPqr(pqr_id, data.fecha_creacion, data.fecha_devolucion, data.observacion, data.contenido, decodedToken.user_id, data.estado, data.tipo_pqr, token.access);

      if (response) {
        //setToken(response);
        alert('PQR Actualizada correctamente');
        navigate("/home", { replace: true });
      } else {
        // Manejar el caso en el que no se obtiene el token
        alert('Un Campo  no es valido');
        console.error('Un Campo  no es valido');
        console.log(Error);
      }
    } catch (error) {
      // Manejar errores
      console.error('Error al intentar actualizar la PQR:', error);
      alert('Alguno de los campos no es valido')
      // setError('typePqr', { type: 'manual', message: 'Alguno de los campos no es valido' });

    }
  };

  interface PqrEditForm {
    dateDev: Date | null,
    dateCreate: Date,
    observation: string,
    content: string,
    // userId: number,
    statePqr: string,
    typePqr: string,
    // garantyId: number,
  }

  return (
    <>
      <Breadcrumb pageName="Modificar una PQR" />
      <div className="w-full p-4 sm:p-12.5 xl:p-17.5">
        <div className="flex flex-col gap-4 ">

          <div className="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
            <div className="border-b border-stroke py-4 px-6.5 dark:border-strokedark">
              <h3 className="font-medium text-black dark:text-white">
                Modificar PQR # {pqr_id}
              </h3>
            </div>
            <form onSubmit={handleSubmit}>

              {/* {Object.keys(data).map((key, index) => ( */}
              <div className="p-6.5 ">
                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">
                  <div className="w-full xl:w-1/2">
                    <div >
                      <label className="mb-3 block text-black dark:text-white">
                        Fecha de creacion
                      </label>
                      <div className="relative">
                        <input
                          type="date"
                          name="fecha_creacion"
                          disabled
                          // {...register('dateCreate', { required: 'Este campo es obligatorio' })}
                          className="custom-input-date custom-input-date-2 w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                          // placeholder={(new Date().toISOString().split('T')[0])}
                          value={data.fecha_creacion}
                          onChange={(e) => {
                            const selectedDate = new Date(e.target.value);
                            const formattedDate = selectedDate.toISOString().split('T')[0];
                          }}

                        />
                      </div>
                    </div>
                  </div>

                  <div className="w-full xl:w-1/2">
                    <div>
                      <label className="mb-3 block text-black dark:text-white">
                        Fecha de Devolución
                      </label>
                      <div className="relative">
                        <input
                          type="date"
                          name="fecha_devolucion"
                          value={data.fecha_devolucion ? data.fecha_devolucion : ''}
                          // {...register('dateDev', {})}
                          className="custom-input-date custom-input-date-2 w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                          onChange={handleChange}
                          disabled={userData.Rol[0] == 'ADMIN' ? false : true}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">
                  <div className="w-full xl:w-1/2">
                    <label className="mb-3 block text-black dark:text-white">
                      Seleccione Tipo de Consulta
                    </label>
                    <div className="relative z-20 bg-white dark:bg-form-input">

                      <span className="absolute top-1/2 left-4 z-30 -translate-y-1/2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24" fill="none" stroke="#b1b9c5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><path d="M13 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V9l-7-7z" /><path d="M13 3v6h6" /></svg>
                      </span>
                      <select
                        // {...register('typePqr')}
                        onChange={handleChange}
                        name="tipo_pqr"
                        disabled={userData.Rol[0] == 'ADMIN' ? false : true}
                        value={data.tipo_pqr}
                        className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-12 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                        {/* <option value="">{data.tipo_pqr}</option> */}
                        <option value="PQR">PQR</option>
                        <option value="GARANTIA">GARANTIA</option>
                      </select>
                      <span className="absolute top-1/2 right-4 z-10 -translate-y-1/2">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <g opacity="0.8">
                            <path
                              fillRule="evenodd"
                              clipRule="evenodd"
                              d="M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z"
                              fill="#637381"
                            ></path>
                          </g>
                        </svg>
                      </span>
                    </div>
                  </div>
                  <div className="w-full xl:w-1/2">
                    <label className="mb-2.5 block text-black dark:text-white">
                      Estado
                    </label>
                    <div>
                      {/* <input
                      type="text"
                      // {...register('statePqr', { required: 'Este campo es obligatorio' })}
                      //placeholder="ABIERTA"

                      value={data.estado}
                      name="estado"
                      onChange={handleChange}
                      disabled={userData.Rol[0] == 'ADMIN' ? false : true}
                      className="w-full rounded-lg border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary dark:disabled:bg-black"
                    />
                    <div className="relative z-20 bg-white dark:bg-form-input"> */}


                      <select
                        // {...register('typePqr')}
                        onChange={handleChange}
                        name="tipo_pqr"
                        disabled={userData.Rol[0] == 'ADMIN' ? false : true}
                        value={data.tipo_pqr}
                        className="relative z-20 w-full appearance-none rounded border border-stroke bg-transparent py-3 px-5 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input">
                        {/* <option value="">{data.tipo_pqr}</option> */}
                        <option value="ABIERTA">ABIERTA</option>
                        <option value="CERRADA">CERRADA</option>
                        <option value="EN PROCESO">EN PROCESO</option>
                      </select>

                    </div>


                  </div>
                </div>
                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">
                </div>
                <div className="mb-6">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Contenido
                  </label>
                  <textarea
                    rows={6}
                    value={data.contenido}
                    name="contenido"
                    // {...register('content', { required: 'Este campo es obligatorio' })}
                    onChange={handleChange}
                    placeholder="Type your message"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  ></textarea>
                </div>
                <div className="mb-6">
                  <label className="mb-2.5 block text-black dark:text-white">
                    Observaciones
                  </label>
                  <textarea
                    rows={6}
                    value={data.observacion}
                    name="observacion"
                    // {...register('observation', { required: 'Este campo es obligatorio' })}
                    onChange={handleChange}
                    placeholder="Type your message"
                    className="w-full rounded border-[1.5px] border-stroke bg-transparent py-3 px-5 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary"
                  ></textarea>
                </div>
                <div className="mb-4.5 flex flex-col gap-2 xl:flex-row">
                  <button className="flex w-full cursor-pointer justify-center rounded bg-primary p-3 font-medium text-gray"
                    type="submit">Guardar</button>
                  <button className="flex w-full justify-center rounded bg-primary p-3 font-medium text-gray"
                    onClick={() => navigate("/pqrs")}>
                    Cancelar
                  </button>
                </div>
              </div>
              {/* ))} */}

            </form>

          </div>
        </div >

      </div >
    </>
  );
};

export default FormPqrEdit;