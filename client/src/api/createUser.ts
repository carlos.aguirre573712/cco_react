import axios from 'axios';
// import { createContext, useState, useEffect } from 'react';
// import { useAuth } from '../context/UserContext';

const BASE_URL = 'http://127.0.0.1:8000/ccoApp';

//let [authTokens, setAuthTokens] = useState(()=> localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : null)

export const createUser = async (
  firstName: string,
  lastName: string,
  celNumber: string,
  mail: string,
  userIdentification: string,
  password: string,
) => {
  try {
    // console.log('celNumber', celNumber);
    const response = await axios.post(`${BASE_URL}/createUser/`, {
      Nombres: firstName,
      Apellidos: lastName,
      Celular: celNumber,
      Email: mail,
      Cedula: userIdentification,
      password: password,
    });

    if (response.status == 200) {
      //setToken(response.data);
      //   setAuthTokens(data)
      //   SpeechSynthesisUtterance(data.access)
      alert('Usuario creado exitosamente');
    }
    // Puedes devolver datos adicionales según sea necesario
    return response.data;
  } catch (error) {
    // Manejar errores
    alert('Upps... Algo salio mal!');
    throw error;
  }
};
