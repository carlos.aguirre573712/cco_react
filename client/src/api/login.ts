import axios from 'axios';
// import { createContext, useState, useEffect } from 'react';
// import { useAuth } from '../context/UserContext';

const BASE_URL = 'http://127.0.0.1:8000';

//let [authTokens, setAuthTokens] = useState(()=> localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : null)

export const login = async (
  userIdentification: string,
  password: string,
  setToken: any,
) => {
  try {
    const response = await axios.post(`${BASE_URL}/login/`, {
      Cedula: userIdentification,
      password: password,
    });

    if (response.status == 200) {
      setToken(response.data);
      //   setAuthTokens(data)
      //   SpeechSynthesisUtterance(data.access)
      alert('Te has logeado correctamente');
    }
    // Puedes devolver datos adicionales según sea necesario
    return response.data;
  } catch (error) {
    // Manejar errores
    alert('Algo salio mal!');
    throw error;
  }
};

/* import axios from 'axios';
import { createContext, useState, useEffect } from 'react';

const BASE_URL = 'http://127.0.0.1:8000';

//let [authTokens, setAuthTokens] = useState(()=> localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : null)

export const login = async (userIdentification: string, password: string) => {
  try {
    const response = await axios.post(`${BASE_URL}/login/`, {
      Cedula: userIdentification,
      password: password,
    });

    if (response.status == 200) {
      //   setAuthTokens(data)
      //   SpeechSynthesisUtterance(data.access)
      alert('Te has logeado correctamente');
    }
    // Puedes devolver datos adicionales según sea necesario
    console.log(response.data);

    return response.data;
  } catch (error) {
    // Manejar errores
    alert('Algo salio mal!');
    throw error;
  }
};
 */
