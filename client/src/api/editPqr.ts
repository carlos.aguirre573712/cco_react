import axios from 'axios';
import { createContext, useState, useEffect } from 'react';
import { useAuth } from '../context/UserContext';
import { useNavigate } from 'react-router-dom';
import { None } from 'framer-motion';

const BASE_URL = 'http://127.0.0.1:8000/ccoApp';

//let [authTokens, setAuthTokens] = useState(()=> localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : null)

export const editPqr = async (
  // dateDev: Date | None,
  idPqr: string,
  dateCreate: Date,
  dateDev: Date,
  observation: string,
  content: string,
  userId: number,
  statePqr: string,
  typePqr: string,
  //   garantyId: number,
  token: string,
) => {
  try {
    const response = await axios.put(
      `${BASE_URL}/updatePqr/${idPqr}`,
      {
        fecha_devolucion: dateDev,
        fecha_creacion: dateCreate,
        observacion: observation,
        contenido: content,
        usuario_id: userId,
        estado: statePqr,
        tipo_pqr: typePqr,
        id_garantia: null,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    console.log('INFO', response.data);
    console.log(response.data.PQR.Usuario.id);

    if (response.status == 200) {
      //setToken(response.data);
      //   setAuthTokens(data)
      //   SpeechSynthesisUtterance(data.access)
      alert('PQR registrada exitosamente');
    }
    // Puedes devolver datos adicionales según sea necesario
    return response.data;
  } catch (error) {
    // Manejar errores
    alert('Upps... Algo salio mal!');
    throw error;
  }
};
