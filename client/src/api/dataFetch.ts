import axios from 'axios';
import { useAuth } from '../context/UserContext';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const BASE_URL = 'http://127.0.0.1:8000/ccoApp';

//let [authTokens, setAuthTokens] = useState(()=> localStorage.getItem('authTokens') ? JSON.parse(localStorage.getItem('authTokens')) : null)

// Función que realiza la segunda petición axios usando el token
export const fetchDataWithToken = async (url: string) => {
  // Obtén el contexto de autenticación
  const { token } = useAuth();
  const navigate = useNavigate();

  console.log('token', token);
  //console.log('useAuth', useAuth());

  try {
    // Realiza la segunda petición axios con el token en los encabezados
    const response = await axios.get(`${BASE_URL}${url}`, {
      headers: {
        Authorization: `Bearer ${token.access}`,
      },
    });

    // Maneja la respuesta según tus necesidades
    console.log('Datos obtenidos con éxito:', response.data);

    // Puedes retornar la respuesta si es necesario
    return response.data;
  } catch (error) {
    // Maneja errores
    navigate('/login', { replace: true });
    console.error('Error al obtener datos con token:', error);
    throw error;
  }
};
