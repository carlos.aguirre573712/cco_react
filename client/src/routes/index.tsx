import { Navigate, RouterProvider, createBrowserRouter } from "react-router-dom";
import { ProtectedRoute } from "./ProtectedRoute";
import { useAuth } from "../context/UserContext";
import Home from "../pages/Dashboard/Home";
import SignIn from "../pages/Authentication/SignIn";
import { lazy } from "react";
import Profile from "../pages/Profile";
import FormElements from "../pages/Form/FormElements";
import Ayuda from "../pages/Ayuda";
import Pqr from "../pages/Consultas/Pqr";
import PqrTable from "../pages/Consultas/PqrTable";
import FormLayout from "../pages/Form/FormLayout";
import FormPqr from "../pages/Form/PQR";
import Settings from "../pages/Settings";
import Soporte from "../pages/Soporte";
import Alerts from "../pages/UiElements/Alerts";
import Buttons from "../pages/UiElements/Buttons";
import Logout from "../pages/Logout";
import SignUp from "../pages/Authentication/SignUp";
import FormPqrEdit from "../pages/Form/PqrEdit";
import DefaultLoginLayout from "../layout/DefaultLogin";


const DefaultLayout = lazy(() => import('../layout/DefaultLayout'));
const Routes = () => {
  const { token } = useAuth();

  // console.log("AAAAAAAAAAAAAAAAAA", token);


  // Define public routes accessible to all users
  const routesForPublic = [
    {
      path: "/service",
      element: <div>Service Page</div>,
    },
    {
      path: "/about-us",
      element: <div>About Us</div>,
    },
  ];

  // Define routes accessible only to authenticated users
  const routesForAuthenticatedOnly = [
    {
      path: "/",
      element: <ProtectedRoute />, // Wrap the component in ProtectedRoute
      children: [
        {
          path: "/Home",
          element: <Home />,
        },
        {
          path: "/profile",
          element: <Profile />,
        },
        {
          path: "/logout",
          element: <Logout />,
        },
        {
          path: '/forms/form-elements',
          element: <FormElements />,
        },
        {
          path: '/forms/form-layout',

          element: <FormLayout />,
        },
        {
          path: '/forms/form-pqr',
          element: <FormPqr />,
        },
        {
          path: "/forms/edit-pqr/:pqr_id",
          element: <FormPqrEdit />
          // path: '/forms/edit-pqr',
          // element: <FormPqrEdit />,
        },

        {
          path: '/pqrs',
          element: <PqrTable />,
        },
        {
          path: '/pqr',
          element: <Pqr />,
        },
        {
          path: '/settings',
          element: Settings,
        },

        {
          path: '/ui/alerts',
          element: <Alerts />,
        },
        {
          path: '/ui/buttons',
          element: <Buttons />,
        },
        {
          path: '/ayuda',
          element: <Ayuda />,
        },
        {
          path: '/support',
          element: <Soporte />,
        }
      ],
    },
  ];

  // Define routes accessible only to non-authenticated users
  const routesForNotAuthenticatedOnly = [
    // {
    //   path: "/",
    //   element: <SignIn />,
    // },
    // {
    //   path: "/login/signin",
    //   element: <SignIn />,
    // },
    // {
    //   path: "/login/signup",
    //   element: <SignUp />,
    // },
    { path: "/support", element: <Soporte /> },
    { path: "/about-us", element: <div>About Us</div> },

  ];

  // Combine and conditionally include routes based on authentication status
  const router = createBrowserRouter(
    [
      {
        path: "auth",
        element: <DefaultLoginLayout />,
        children: [

          { path: "signin", element: <SignIn /> },
          { path: "signup", element: <SignUp /> },
        ],
      },
      {
        path: "/signin",
        element: !token ? <SignIn /> : <Navigate to="/" />
      },
      {
        path: "/",
        element: <DefaultLayout />,
        children: [
          // { path: "/", element: <SignIn /> },
          // { path: "/", element: <SignUp /> },

          ...(!token ? routesForNotAuthenticatedOnly : []),
          ...routesForAuthenticatedOnly,

        ],
      },

      //...routesForAuthenticatedOnly,
      //...(!token ? routesForNotAuthenticatedOnly : []),

    ]);

  // Provide the router configuration using RouterProvider
  return <RouterProvider router={router} />;
};

export default Routes;

// import { lazy } from 'react';
// import FormPqr from '../pages/Form/PQR';
// import PqrTable from '../pages/Consultas/PqrTable';
// import Pqr from '../pages/Consultas/Pqr';
// import Ayuda from '../pages/Ayuda';
// import Soporte from '../pages/Soporte';

// const Home = lazy(() => import('../pages/Dashboard/Home'));
// const Calendar = lazy(() => import('../pages/Calendar'));
// const Chart = lazy(() => import('../pages/Chart'));
// const FormElements = lazy(() => import('../pages/Form/FormElements'));
// const FormLayout = lazy(() => import('../pages/Form/FormLayout'));
// const Profile = lazy(() => import('../pages/Profile'));
// const Settings = lazy(() => import('../pages/Settings'));
// const Tables = lazy(() => import('../pages/Tables'));
// const Alerts = lazy(() => import('../pages/UiElements/Alerts'));
// const Buttons = lazy(() => import('../pages/UiElements/Buttons'));


// const coreRoutes = [
//   {
//     path: '/home',
//     title: 'Home',
//     component: Home,
//   },

//   {
//     path: '/calendar',
//     title: 'Calender',
//     component: Calendar,
//   },
//   {
//     path: '/profile',
//     title: 'Profile',
//     component: Profile,
//   },
//   {
//     path: '/forms/form-elements',
//     title: 'Forms Elements',
//     component: FormElements,
//   },
//   {
//     path: '/forms/form-layout',
//     title: 'Form Layouts',
//     component: FormLayout,
//   },
//   {
//     path: '/forms/form-pqr',
//     title: 'Crear PQR',
//     component: FormPqr,
//   },

//   {
//     path: '/pqrs',
//     title: 'Mis PQR',
//     component: PqrTable,
//   },
//   {
//     path: '/pqr',
//     title: 'PQR',
//     component: Pqr,
//   },
//   {
//     path: '/settings',
//     title: 'Settings',
//     component: Settings,
//   },
//   {
//     path: '/chart',
//     title: 'Chart',
//     component: Chart,
//   },
//   {
//     path: '/ui/alerts',
//     title: 'Alerts',
//     component: Alerts,
//   },
//   {
//     path: '/ui/buttons',
//     title: 'Buttons',
//     component: Buttons,
//   },
//   {
//     path: '/ayuda',
//     title: 'Ayuda',
//     component: Ayuda,
//   },
//   {
//     path: '/support',
//     title: 'Buttons',
//     component: Soporte,
//   },
// ];

// const routes = [...coreRoutes];
// export default routes;
