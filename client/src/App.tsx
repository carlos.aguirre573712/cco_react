import { useEffect, useState } from 'react';
import Routes from "./routes";
import { Toaster } from 'react-hot-toast';

import Loader from './common/Loader';
import AuthProvider from './context/UserContext';
// import UserDataProvider from './context/UserDataContext'

function App() {
  const [loading, setLoading] = useState<boolean>(true);


  useEffect(() => {
    setTimeout(() => setLoading(false), 1000);
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <>
      <Toaster
        position="top-right"
        reverseOrder={false}
        containerClassName="overflow-auto"
      />
      <AuthProvider>
        {/* <UserDataProvider> */}
        <Routes />
        {/* </UserDataProvider> */}
      </AuthProvider>
    </>
  );
}

export default App;



// import { Suspense, lazy, useEffect, useState } from 'react';
// import { Route, Routes } from 'react-router-dom';
// import { Toaster } from 'react-hot-toast';

// import SignIn from './pages/Authentication/SignIn';
// import SignUp from './pages/Authentication/SignUp';
// import Recover from './pages/Authentication/Recover';
// import Loader from './common/Loader';
// import routes from './routes';
// import Home from './pages/Dashboard/Home';

// const DefaultLayout = lazy(() => import('./layout/DefaultLayout'));

// function App() {
//   const [loading, setLoading] = useState<boolean>(true);

//   useEffect(() => {
//     setTimeout(() => setLoading(false), 1000);
//   }, []);

//   return loading ? (
//     <Loader />
//   ) : (
//     <>
//       <Toaster
//         position="top-right"
//         reverseOrder={false}
//         containerClassName="overflow-auto"
//       />
//       <Routes>
//         <Route path="/auth/signin" element={<SignIn />} />
//         <Route path="/auth/signup" element={<SignUp />} />
//         <Route path="/auth/recover" element={<Recover />} />

//         <Route element={<DefaultLayout />}>
//           <Route index element={<Home />} />
//           {routes.map((routes, index) => {
//             const { path, component: Component } = routes;
//             return (
//               <Route
//                 key={index}
//                 path={path}
//                 element={
//                   <Suspense fallback={<Loader />}>
//                     <Component />
//                   </Suspense>
//                 }
//               />
//             );
//           })}
//         </Route>
//       </Routes>
//     </>
//   );
// }

// export default App;
