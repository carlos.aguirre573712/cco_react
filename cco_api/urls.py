"""
URL configuration for cco_api project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from ccoApp import views


# CONFIGURACION DE LAS URL DEL PROYECTO

urlpatterns = [
    path("", views.View, name="front"),
    # path("admin/", admin.site.urls),
    path("login/", TokenObtainPairView.as_view()),
    path("refresh/", TokenRefreshView.as_view()),
    path("validarUsuario/", views.ValidacionView.as_view()),
    # CRUD ROL
    # path("ccoApp/createRol/", views.RolView.as_view()),
    # path("ccoApp/deleteRol/<int:pk>", views.RolView.as_view()),
    # path("ccoApp/updateRol/<int:pk>", views.RolView.as_view()),
    # path("ccoApp/rols/<int:pk>", views.RolView.as_view()),
    # path("ccoApp/rols/", views.AllRols.as_view()),
    # CRUD USUARIO
    path("ccoApp/createUser/", views.UsuarioCreate.as_view()),
    path("ccoApp/deleteUser/", views.UsuarioView.as_view()),
    path("ccoApp/updateUser/", views.UsuarioView.as_view()),
    path("ccoApp/User/", views.UsuarioView.as_view()),
    path("ccoApp/Users/", views.AllUsuario.as_view()),
    # CRUD USUARIO ID
    path("ccoApp/deleteUser/<int:pk>", views.UsuarioId.as_view()),
    path("ccoApp/updateUser/<int:pk>", views.UsuarioId.as_view()),
    path("ccoApp/User/<int:pk>", views.UsuarioId.as_view()),
    # CRUD ALMACEN
    path("ccoApp/createAlmacen/", views.AlmacenView.as_view()),
    path("ccoApp/deleteAlmacen/<int:pk>", views.AlmacenView.as_view()),
    path("ccoApp/updateAlmacen/<int:pk>", views.AlmacenView.as_view()),
    path("ccoApp/Almacen/<int:pk>", views.AlmacenView.as_view()),
    path("ccoApp/Almacens/", views.AllAlmacen.as_view()),
    # CRUD GARANTIA
    path("ccoApp/createGarantia/", views.GarantiaView.as_view()),
    path("ccoApp/deleteGarantia/<int:pk>", views.GarantiaView.as_view()),
    path("ccoApp/updateGarantia/<int:pk>", views.GarantiaView.as_view()),
    path("ccoApp/Garantia/<int:pk>", views.GarantiaView.as_view()),
    path("ccoApp/Garantias/", views.AllGarantia.as_view()),
    # CRUD PQR
    path("ccoApp/createPqr/", views.PqrView.as_view()),
    path("ccoApp/deletePqr/<int:pk>", views.PqrView.as_view()),
    path("ccoApp/updatePqr/<int:pk>", views.PqrView.as_view()),
    path("ccoApp/Pqr/", views.PqrView.as_view()),
    path("ccoApp/Pqrs/", views.AllPqr.as_view()),
    path("ccoApp/PqrId/<int:pk>", views.PqrId.as_view()),
    # CRUD RESPUESTA
    path("ccoApp/createRespuesta/", views.RespuestaView.as_view()),
    path("ccoApp/deleteRespuesta/<int:pk>", views.RespuestaView.as_view()),
    path("ccoApp/updateRespuesta/<int:pk>", views.RespuestaView.as_view()),
    path("ccoApp/Respuesta/<int:pk>", views.RespuestaView.as_view()),
    path("ccoApp/Respuestas/", views.AllRespuesta.as_view()),
]
